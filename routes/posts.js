const express = require('express');

const { body } = require('express-validator');
const imageController = require('../controllers/image.controller');
const imageUploader = require('../helpers/image-uploader');
const postsController = require('../controllers/posts');

const auth = require('../middleware/auth');
const conx=require('../util/database')

const router = express.Router();

router.get('/', postsController.fetchAll);

router.post(
  '/add',
  [
    body('description').trim().isLength({ min: 5 }).not().isEmpty(),
    body('username').trim().not().isEmpty(),
    body('likes').trim(),
    body('vues').trim(),
 
    
    //image

  ],
  postsController.postPost
);
router.post('/upload',  imageUploader.upload.single('image'), imageController.upload);

router.post('/updatelikes',(req, res) => {
  const userId = req.body.id;
    let sql = "update photo SET   likes='"+req.body.likes+"' where id ="+userId;
    let query = conx.query(sql,(err, results) => {
      if(err) throw err;
      if(results){
        res.status(202).json(
          "user updated"
        )
        
      }
    });
  
});
router.post('/update',(req, res) => {
  const userId = req.body.id;
    let sql = "update photo SET   likes='"+req.body.likes+"', username='"+req.body.username+"', vues='"+req.body.vues+"', description='"+req.body.description+"' where id ="+userId;
    let query = conx.query(sql,(err, results) => {
      if(err) throw err;
      if(results){
        res.status(202).json(
          "user updated"
        )
        
      }
    });
  
});
router.get('/get/:id' , (req, res) => {
  let id = req.params.id;
  let sql = `SELECT * FROM photo WHERE id = ${id}` ;
  conx.query(sql, (err, rows, fields) => {
    console.log("houda1")

    if (rows)
    res.status(202).json(
      rows
    )
  else
  console.log(err);
  })
} );

router.delete('/:id',  postsController.deletePost);

module.exports = router;
