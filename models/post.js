const db = require('../util/database');

module.exports = class Post {
  constructor(description, username, likes, vues, img_url) {
    this.description = description;
      this.username = username;
      this.likes = likes;
      this.vues =vues;
      this.img_url=img_url;
  }

  static fetchAll() {
    return db.execute('SELECT * FROM photo');
  }

  static save(post) {
    return db.execute(
      'INSERT INTO photo (description, username, likes, vues, img_url ) VALUES (?, ?, ?, ?, ?)',
      [post.description, post.username, post.likes, post.vues, post.img_url]
    );
  }

  static delete(id) {
    return db.execute('DELETE FROM photo WHERE id = ?', [id]);
  }
};
